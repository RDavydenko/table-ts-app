import config from "./app.json";

interface AppConfig {
    api: string
}

export default config as AppConfig;