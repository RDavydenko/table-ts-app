export interface IModalProps {
    visible: boolean,
    setVisible: (visible: boolean) => void,
    onClose?: () => void
}
