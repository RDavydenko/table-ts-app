import { Button, Space } from 'antd'
import React, { useState } from 'react'
import CreateUserModal from '../components/users/CreateUserModal';
import UsersTable from '../components/users/UsersTable'

const UsersPage = () => {
    const [modalVisible, setModalVisible] = useState(false);

    return (
        <div>
            <CreateUserModal visible={modalVisible} setVisible={setModalVisible}/>
            <Space align="center" style={{ marginBottom: "16px"}}>
                <Button onClick={() => setModalVisible(true)}>Добавить</Button>
            </Space>
            <UsersTable />
        </div>
    )
}

export default UsersPage
