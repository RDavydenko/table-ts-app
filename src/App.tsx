import { Breadcrumb, Button, Menu } from 'antd';
import Layout, { Content, Footer, Header } from 'antd/lib/layout/layout';
import React from 'react';
import './App.css';
import UsersPage from './pages/UsersPage';

const App = () => {
  return (
    <Layout>
      <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
      <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
        <Menu.Item key="1">Users</Menu.Item>
        <Menu.Item key="2" disabled>Other</Menu.Item>
        <Menu.Item key="3" disabled>Other</Menu.Item>
      </Menu>
    </Header>
    <Content className="site-layout" style={{ padding: '0 50px', marginTop: 64 }}>
      <Breadcrumb style={{ margin: '16px 0' }}>
        <Breadcrumb.Item>Home</Breadcrumb.Item>
        <Breadcrumb.Item>Users</Breadcrumb.Item>
      </Breadcrumb>
      <div className="site-layout-background" style={{ padding: 24, minHeight: 380 }}>
        <UsersPage />
      </div>
    </Content>
    <Footer style={{ textAlign: 'center' }}>React + TypeScript + MobX + AntD</Footer>
  </Layout>
  );
}

export default App;
