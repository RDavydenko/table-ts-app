import axios from "axios";
import AppConfig from "../config";
import { IUser } from "../models";

class UserService {
    static async getAll(): Promise<IUser[]> {
        return (await axios.get(AppConfig.api + "users")).data;
    }
}

export default UserService;