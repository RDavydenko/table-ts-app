import { Button, Table } from 'antd';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react'
import { IUser } from '../../models';
import UserStore from '../../stores/UserStore';

const UsersTable = observer(() => {
    const columns = [
        {
            title: 'ID',
            dataIndex: "id",
            key: 'id'
          },
        {
          title: 'Name',
          dataIndex: "name",
          key: 'name'
        },
        {
          title: 'Email',
          dataIndex: 'email',
          key: 'email',
        },
        {
          title: 'Action',
          key: 'action',
          render: (_: any, record: IUser) => (
              <Button onClick={() => removeUser(record.id)}>Delete</Button>
          ),
        },
    ];

    useEffect(() => {
        UserStore.fetchUsers();
    }, []);

    const removeUser = async (id: number) => {
        await UserStore.removeUser(id);
    };

    return (
        <Table<IUser>
            columns={columns}
            dataSource={UserStore.users}
            rowKey={(record) => record.id}
            loading={UserStore.loading}
            pagination={false}
        />
    )
});

export default UsersTable;
