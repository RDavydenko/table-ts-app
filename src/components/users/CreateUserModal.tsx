import { Button, Input } from 'antd'
import Modal from 'antd/lib/modal/Modal'
import React, { useState } from 'react'
import { IModalProps, IUser } from '../../models'
import UserStore from '../../stores/UserStore'

const CreateUserModal = ({ visible, setVisible }: IModalProps) => {
    const [user, setUser] = useState<IUser>({} as IUser);
    
    const submit = async () => {
        await UserStore.addUser(user);
        close();    
    };

    const cancel = () => {
        close();
    }

    const close = () => {
        setVisible(false);
        setUser({} as IUser);
    }

    return (
        <div>
            <Modal title="Добавить пользователя" visible={visible} onOk={submit} onCancel={cancel}>
                <Input
                    type="text"
                    placeholder="Name"
                    style={{ marginBottom: 5 }}
                    value={user.name}
                    onChange={(e) => setUser({ ...user, name: e.target.value })} 
                />
                <Input
                    type="email"
                    placeholder="Email"
                    value={user.email}
                    onChange={(e) => setUser({ ...user, email: e.target.value })} 
                />
            </Modal>
        </div>
    )
}

export default CreateUserModal
