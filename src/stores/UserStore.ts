import { makeAutoObservable } from "mobx";
import { IUser } from "../models";
import UserService from "../services/UserService";

class UserStore {
    users: IUser[] = []
    loading: boolean = false;

    constructor() {
        makeAutoObservable(this);
    }

    async fetchUsers(): Promise<void> {
        try {
            this.loading = true;
            this.users = await UserService.getAll();
        } catch (e) {
            console.error(e);
        } finally {
            this.loading = false;
        }
    }

    async removeUser(id: number): Promise<void> {
        this.loading = true;
        setTimeout(() => {
            this.users = this.users.filter(x => x.id !== id);
            this.loading = false;
        }, 1000);
    }

    async addUser(user: IUser) {
        this.loading = true;
        setTimeout(() => {
            const id = this.users.map(x => x.id).sort((a, b) => b - a)[0] + 1;
            this.users = [...this.users, { ...user, id: id }];
            this.loading = false;
        }, 1000);
    }
}

export default new UserStore();